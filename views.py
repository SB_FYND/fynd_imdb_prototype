import os
import sys
import time

import MySQLdb as mdb

import logging
import datetime

# general variables
HOST = "localhost"
USER = "root"
PASSWD = "skynet@123"
DB_NAME = "proxyserver_db"


def edit_movie_details():
    pass

def add_movie():
    pass

def remove_movie():
    pass

def view_movie_details():
    pass

def admin_login():
    pass

def user_login():
    pass

def search_movie():
    pass

def search_by_popularity():
    pass

def search_by_director():
    pass

def search_by_name():
    pass

def search_by_score():
    pass

def apply_filter():
    pass

def filter_by_adventure():
    pass

def filter_by_family():
    pass


def establish_db_con():
    con = mdb.connect(HOST, USER, PASSWD, DB_NAME)
    cur = con.cursor()
    return cur

def close_db_con(cur):
    cur.close()
    return True

def test_db_con():
    cur = establish_db_con()
    cur.execute("select * from user_details")
    data = cur.fetchall()
    if close_db_con(cur) == True:
        print "DB connection closed successfully"

test_db_con()
